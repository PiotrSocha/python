import random
from art import logo
print(logo)
print("Welcome to the Number Guessing Game!")
print("I'm thinking of a number between 1 and 100")
number = random.randint(1, 101)
# print(number)
level = input("Choose a difficulty. type 'easy' or 'hard': ")

def game(attempt):
    game_over = False
    if level == "easy":
        attempts = 10
    else:
        attempts = 5

    while not game_over:
        print(f"You have {attempts} remaining to guess the number.")
        guess = int(input("Make a guess: "))
        attempts -= 1
        if guess > number:
            print("Too high. \nGuess again.")
        if guess < number:
            print("To low. \nGuess again.")
        if guess == number:
            print(f"You guessed it! The number was {number}")
            game_over = True
        elif attempts == 0:
            print(f"You lost! The number was {number}")
            game_over = True
game(level)